<h1>![Navi logo](img/navi256x256.png){width=64px height=64px} Navi</h1>

A tiny tool for exploring Mastodon-like account archives entirely in a static web page.

## Features
- View all the toots of your archive with human-readable format.
- Fast search for specific terms in your toots.
- View all your medias in a gallery.
- Some fun stats about your account.
- All these features without needing to upload your archive, everything is done locally from your compressed archive.

## Use of your data
Your archive is processed entirely locally by your own device. Navi doesn't have any backend server of any kind, everything is in your browser.<br>
Navi doesn't use cookies for its features. Collected data about you (IP address, cookies, ...) may vary depending on the host of the Navi instance you use. The safest option is to run Navi locally if you absolutely want none of your data to leave your device.